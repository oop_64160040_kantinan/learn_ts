interface Retangle {
    width: number;
    height: number;
}

interface ColoredRetangle extends Retangle{
    color: string
}

const retangle: Retangle ={
    width: 20,
    height: 10
}
console.log(retangle)

const coloredRetangle: ColoredRetangle = {
    width: 20,
    height: 10,
    color: "red"
}

console.log(coloredRetangle)